# Common shell helpers

alias random='od -vAn -N4 -tu4 < /dev/urandom'

#*****************************************************************************************************

# Unix & Sys-V aliases.

alias a2reload='service apache2 reload'
